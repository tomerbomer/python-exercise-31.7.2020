firstList = [3, 5, 30, 0]
secondList = [1000, 5, 29, -5, 4, 2]
count = 0  # count == 0 -> lists equals. count > 0 -> firstList bigger. count < 0 -> secondList bigger.

listLen = len(secondList) if len(firstList) > len(secondList) else len(firstList)

for i in range(0, listLen):
    if firstList[i] > secondList[i]:
        count += 1
    elif firstList[i] < secondList[i]:
        count -= 1

if count > 0:
    print("First list has bigger numbers.")
elif count < 0:
    print("Second list has bigger numbers.")
else:
    print("No list has bigger numbers than the other.")

numSum = 0
numAvg = 0
count = 1
userNum = int(input("Please enter number #1: "))

while userNum >= 0:
    numSum += userNum
    numAvg = round(numSum / count, 2)
    count += 1
    userNum = int(input(f"Please enter number #{count} (avg={numAvg}. sum={numSum}): "))
print("Thank you, goodbye.")

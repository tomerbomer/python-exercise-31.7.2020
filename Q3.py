wordList = []

while True:
    newWord = input("Enter word: ").upper()
    wordList.append(newWord)
    if wordList.count(newWord) == 3:
        print(f"You entered the word {newWord} three times. Goodbye.")
        break

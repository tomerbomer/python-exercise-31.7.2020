userNum = int(input("Enter a number: "))
divList = []
for num in range(1, userNum):
    if not userNum % num:  # userNum % num > 0 -> userNum doesn't divide by num
        divList.append(num)
print(f"{userNum} divides by: {divList}")
